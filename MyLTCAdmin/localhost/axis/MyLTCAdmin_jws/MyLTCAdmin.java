/**
 * MyLTCAdmin.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package localhost.axis.MyLTCAdmin_jws;

public interface MyLTCAdmin extends java.rmi.Remote {
    public boolean setOffset(java.lang.String user, java.lang.String pwd, java.lang.String location, double offset) throws java.rmi.RemoteException;
    public boolean addLocation(java.lang.String user, java.lang.String pwd, java.lang.String location, double offset) throws java.rmi.RemoteException;
    public int callCount(java.lang.String user, java.lang.String pwd) throws java.rmi.RemoteException;
}
