/**
 * MyLTCAdminService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package localhost.axis.MyLTCAdmin_jws;

public interface MyLTCAdminService extends javax.xml.rpc.Service {
    public java.lang.String getMyLTCAdminAddress();

    public localhost.axis.MyLTCAdmin_jws.MyLTCAdmin getMyLTCAdmin() throws javax.xml.rpc.ServiceException;

    public localhost.axis.MyLTCAdmin_jws.MyLTCAdmin getMyLTCAdmin(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
