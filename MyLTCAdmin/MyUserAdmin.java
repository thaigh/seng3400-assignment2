import localhost.axis.MyLTCAdmin_jws.*;

public class MyUserAdmin {

	private static MyLTCAdmin adminServer;

	public static void main(String[] args) {

		// Check for method name
		if (args.length < 1) {
			System.err.println("Error: No API method supplied");
			System.err.println("Expected: 'MyUserAdmin methodName arguments'");
			System.exit(1);
		}

		try {
			MyLTCAdminService service = new MyLTCAdminServiceLocator();
			adminServer = service.getMyLTCAdmin();

			// Get the method name
			String methodName = args[0];
			switch (methodName.toLowerCase()) {
					case "addlocation": {
						addLocation(args);
						break;
					}
					case "setoffset": {
						setOffset(args);
						break;
					}
					case "callcount": {
						callCount(args);
						break;
					}
					default: {
						System.err.println("Error: API method is not defined");
						break;
					}
			}

		} catch (Exception e) {
			System.err.println("Error: API Service unavailable. " + e.getMessage());
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}

	private static void addLocation(String[] args) throws Exception {
			// Check for valid argument length
			if (args.length != 5) {
				System.err.println("Error: Invalid number of arguments");
				System.err.println("Expected: 'MyUserAdmin addLocation username password location offsetValue'");
				return;
			}

			String username = args[1];
			String password = args[2];
			String location = args[3];
			String offsetString = args[4];

			double offsetVal = 0;
			try { offsetVal = Double.parseDouble(offsetString); }
			catch (Exception e) {
				System.err.println("Error: Invalid offset value");
				System.err.println("Expected: A floating point number");
				return;
			}

			if(adminServer.addLocation(username, password, location, offsetVal)) {
				System.out.println("Successfully added " + location + " with offset value " + offsetVal);
			} else {
				System.out.println("Unable to add location. The username and password " +
					"combination may be incorrect or the location may already exist");
			}
	}

	private static void setOffset(String[] args) throws Exception {
		// Check for valid argument length
		if (args.length != 5) {
			System.err.println("Error: Invalid number of arguments");
			System.err.println("Expected: 'MyUserAdmin setOffset username password location offsetValue'");
			return;
		}

		String username = args[1];
		String password = args[2];
		String location = args[3];
		String offsetString = args[4];

		double offsetVal = 0;
		try { offsetVal = Double.parseDouble(offsetString); }
		catch (Exception e) {
			System.err.println("Error: Invalid offset value");
			System.err.println("Expected: A floating point number");
			return;
		}

		if(adminServer.setOffset(username, password, location, offsetVal)) {
			System.out.println("Successfully updated " + location + " to new offset value " + offsetVal);
		} else {
			System.out.println("Unable to set offset value. The username and password " +
				"combination may be incorrect or the location may not exist");
		}
	}

	private static void callCount(String[] args) throws Exception {
		// Check for valid argument length
		if (args.length != 3) {
			System.err.println("Error: Invalid number of arguments");
			System.err.println("Expected: 'MyUserAdmin callCount username password'");
			return;
		}

		String username = args[1];
		String password = args[2];

		int callCount = adminServer.callCount(username, password);
		if(callCount == -1) {
			System.out.println("Unable to get call count. The username and password " +
				"combination may be incorrect");
		} else {
				System.out.println("Call Count: " + callCount);
		}

	}

}
