import localhost.axis.MyLTCServer_jws.*;

public class MyUserClient {

	private static MyLTCServer server;

	public static void main(String[] args) {

		// Check for method name
		if (args.length < 1) {
			System.err.println("Error: No API method supplied");
			System.err.println("Expected: 'MyUserClient methodName arguments'");
			System.exit(1);
		}

		// Set the server
		try {
			MyLTCServerService service = new MyLTCServerServiceLocator();
			server = service.getMyLTCServer();
			// Get method name
			String methodName = args[0];
			switch (methodName.toLowerCase()) {
				case "currentoffset" : {
					currentOffset(args);
					break;
				}
				case "listlocations": {
					System.out.println(server.listLocations());
					break;
				}
				case "convert": {
					convert(args);
					break;
				}
				default: {
					System.err.println("Error: API method is not defined");
					break;
				}
			}

		} catch (Exception e) {
			System.err.println("Error: API Service unavailable. " + e.getMessage());
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}

	private static void currentOffset(String[] args) throws Exception {
			// Get the location name
			if (args.length != 2) {
				System.err.println("Error: Invalid number of arguments");
				System.err.println("Expected: 'MyUserClient currentOffset locationName'");
				return;
			}

			String locationName = args[1];
			System.out.println(server.currentOffset(locationName));
	}

	private static void convert(String[] args) throws Exception {
		if (args.length != 4) {
				System.err.println("Error: Invalid number of arguments");
				System.err.println("Expected: 'MyUserClient convert locationFrom locationTo locationFromTime'");
				return;
		}

		String locationFrom = args[1];
	  	String locationTo = args[2];
	  	String time = args[3];

		System.out.println(server.convert(locationFrom, locationTo, time));
	}

}
