/**
 * MyLTCServerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package localhost.axis.MyLTCServer_jws;

public class MyLTCServerServiceLocator extends org.apache.axis.client.Service implements localhost.axis.MyLTCServer_jws.MyLTCServerService {

    public MyLTCServerServiceLocator() {
    }


    public MyLTCServerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MyLTCServerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MyLTCServer
    private java.lang.String MyLTCServer_address = "http://localhost:8080/axis/MyLTCServer.jws";

    public java.lang.String getMyLTCServerAddress() {
        return MyLTCServer_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MyLTCServerWSDDServiceName = "MyLTCServer";

    public java.lang.String getMyLTCServerWSDDServiceName() {
        return MyLTCServerWSDDServiceName;
    }

    public void setMyLTCServerWSDDServiceName(java.lang.String name) {
        MyLTCServerWSDDServiceName = name;
    }

    public localhost.axis.MyLTCServer_jws.MyLTCServer getMyLTCServer() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MyLTCServer_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMyLTCServer(endpoint);
    }

    public localhost.axis.MyLTCServer_jws.MyLTCServer getMyLTCServer(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            localhost.axis.MyLTCServer_jws.MyLTCServerSoapBindingStub _stub = new localhost.axis.MyLTCServer_jws.MyLTCServerSoapBindingStub(portAddress, this);
            _stub.setPortName(getMyLTCServerWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMyLTCServerEndpointAddress(java.lang.String address) {
        MyLTCServer_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (localhost.axis.MyLTCServer_jws.MyLTCServer.class.isAssignableFrom(serviceEndpointInterface)) {
                localhost.axis.MyLTCServer_jws.MyLTCServerSoapBindingStub _stub = new localhost.axis.MyLTCServer_jws.MyLTCServerSoapBindingStub(new java.net.URL(MyLTCServer_address), this);
                _stub.setPortName(getMyLTCServerWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MyLTCServer".equals(inputPortName)) {
            return getMyLTCServer();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://localhost:8080/axis/MyLTCServer.jws", "MyLTCServerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://localhost:8080/axis/MyLTCServer.jws", "MyLTCServer"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MyLTCServer".equals(portName)) {
            setMyLTCServerEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
