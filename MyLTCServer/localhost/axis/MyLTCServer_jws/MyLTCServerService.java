/**
 * MyLTCServerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package localhost.axis.MyLTCServer_jws;

public interface MyLTCServerService extends javax.xml.rpc.Service {
    public java.lang.String getMyLTCServerAddress();

    public localhost.axis.MyLTCServer_jws.MyLTCServer getMyLTCServer() throws javax.xml.rpc.ServiceException;

    public localhost.axis.MyLTCServer_jws.MyLTCServer getMyLTCServer(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
