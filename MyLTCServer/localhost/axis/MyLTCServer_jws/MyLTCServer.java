/**
 * MyLTCServer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package localhost.axis.MyLTCServer_jws;

public interface MyLTCServer extends java.rmi.Remote {
    public java.lang.String convert(java.lang.String from, java.lang.String to, java.lang.String time) throws java.rmi.RemoteException;
    public java.lang.String listLocations() throws java.rmi.RemoteException;
    public double currentOffset(java.lang.String location) throws java.rmi.RemoteException;
}
