/**
 * Presents an API for the service administrator
 */
public class MyLTCAdmin {

	private static final String USERNAME = "admin";
	private static final String PASSWORD = "converter";

	/**
	 * Add a new location to the system with its current
	 * time offset, relative to GMT. This is not in
	 * hours and minutes (HH:mm) format
	 */
	public boolean addLocation(String user, String pwd,
		String location, double offset) {
		LTCManager manager = new LTCManager();

		// Check user credentials
		// Return false if invalid
		if (!user.equals(USERNAME) || !pwd.equals(PASSWORD))
			return false;

		return manager.addLocation(location, offset);
	}

	/**
	 * Update the time offset for a specific location
	 * This is not in hours and minutes (HH:mm) format
	 */
	public boolean setOffset(String user, String pwd,
		String location, double offset) {
		LTCManager manager = new LTCManager();

		// Check user credentials
		// Return false if invalid
		if (!user.equals(USERNAME) || !pwd.equals(PASSWORD))
			return false;

		return manager.updateLocation(location, offset);
	}

	/**
	 * Retrieve the number of client web service calls
	 * performed on either interface since the server
	 * started.
	 *
	 * Note: In the event of invalid credentials, 
	 * the call count is incremented 
	 */
	public int callCount(String user, String pwd) {
		LTCManager manager = new LTCManager();

		// Check user credentials
		// Return -1 if invalid
		if (!user.equals(USERNAME) || !pwd.equals(PASSWORD))
			return -1;

		// Return total number of calls (including this one)
		return manager.getCallCounts();
	}

}
