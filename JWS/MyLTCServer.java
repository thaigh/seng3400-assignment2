/**
 * Presents API for the Service's users
 */
public class MyLTCServer {

	/**
	 * Retrieve the time offset (relative to GMT)
	 * for a specific location
	 */
	public double currentOffset(String location) {
		LTCManager manager = new LTCManager();
	  return manager.locationOffset(location);
	}

	/**
	 * Retrieve a list of curretly supported locations
	 */
	public String listLocations() {
    	LTCManager manager = new LTCManager();

		// Put all "database" locations into a String
		String res = "";
		for (String loc : manager.locations()) {
			res += loc + "\n";
		}

		// Return String
		return res.substring(0, res.length()-1);
	}

	/**
	 * Convert a time in one location to the time in another
	 */
	public String convert(String from, String to, String time) {
		LTCManager manager = new LTCManager();

		// Check they both exist
		if (!manager.contains(from) || !manager.contains(to))
			return "Invalid location name. Location does not exist";

		// Check that time is in correct format
		// Find the colon
		int colonPosition = time.indexOf(":");
		if (colonPosition < 0) return "Invalid time format. Expected HH:mm";
		if (time.substring(colonPosition+1, time.length()).length() < 2)
			return "Invalid minutes format. Expected HH:mm";

		// Try and parse the hours and minutes
		int hours = 0;
		int minutes = 0;
		try {
			hours = Integer.parseInt(time.substring(0, colonPosition));
			minutes = Integer.parseInt(time.substring(colonPosition+1, time.length()));
		} catch (Exception e) { return "Invalid time format. Expected HH:mm"; }

		// Check for valid value ranges
		if (hours < 0 || hours > 23) return "Invalid hours format. Expected value in range 0 to 23";
		if (minutes < 0 || minutes > 59) return "Invalid minutes format. Expected value in range 0 to 59";

		// Get their time offsets
		double locationFromOffset = manager.locationOffset(from);
		double locationToOffset = manager.locationOffset(to);
		double difference = locationToOffset - locationFromOffset;

		// Convert inputTime to toLoc.TimeOffset
		double toHours = (hours + (int)difference) % 24;
		double toMinutes = minutes + ((difference % 1) * 60);
		
		toHours = (toHours < 0) ? toHours + 24 : toHours;
		if (toMinutes < 0) toHours -= 1;
		toMinutes = (toMinutes < 0) ? toMinutes + 60 : toMinutes;

		return String.format("%02d", (int)toHours) + ":" + String.format("%02d", (int)toMinutes);
	}
}
