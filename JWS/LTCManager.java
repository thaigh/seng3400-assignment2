import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;


public class LTCManager {

	private static HashMap<String, Double> database = new HashMap<String, Double>() {{
		put("SYDNEY",      10.0);
		put("AMSTERDAM",    2.0);
		put("LOS ANGELES", -7.0);
		put("BEIJING",      8.0);
	}};

	private static int callCount;

	public LTCManager() {
		callCount++;
	}

	public boolean addLocation(String location, double offset) {
		// Check if location exists in "database"
		// If yes, return false
		if (database.containsKey(location)) { return false; }
		// Add location to "database"
		// Return true
		else { database.put(location, offset); return true; }
	}

	public boolean contains(String location) {
		return database.containsKey(location);
	}

	public List<String> locations() {
		List<String> locations = new LinkedList<String>();

		for (String loc : database.keySet()) {
			locations.add(loc);
		}
		return locations;
	}

	public double locationOffset(String location) {
		// Lookup location in "database"
		if (contains(location))
			// If present, return offset
			return database.get(location);
		else
			// Else return Not a Number
			return Double.NaN;
	}

	public boolean updateLocation(String location, double offset) {
		// Check if location exists in "database"
		// If no, return false
		if (!contains(location)) return false;
		else {
			// Update "database" entry
			database.put(location, offset);
			// Return true
			return true;
		}
	}

	public int getCallCounts() {
		return LTCManager.callCount;
	}

}
