SENG3400 - Assignment 2
=======================

### Author ###

Tyler Haigh - C3182929

# Abstract #

This project is developed as an assignment for SENG3400 - Network and Distributed Systems at the University of Newcastle, 2015.

This project aims to implement a basic Local Time Converter Java Web Service using Tomcat 8 and Axis 1. Two webservices are supported: `LTCService` for standard user API operations, and `LTCAdmin` for adding and updating locations in the web service's database

# Java Version #

This project has been built using `javac 1.8.0_40` and tested using:

```
java version "1.8.0_60"
Java(TM) SE Runtime Environment (build 1.8.0_60-b27)
Java HotSpot(TM) Client VM (build 25.60-b23, mixed mode, sharing)
```

# Included Files #

The `JWS` Folder contains all of the java web service api interfaces to be presented on the Tomcat server. Included with these is the `LTCManager` which is responsible for managing the database shared between the web services.

The `MyLTCAdmin` folder contains the client side command line application for accessing the `LTCAdmin` web service

The `MyLTCServer` folder contains the client side command line application for accessing the `LTCServer` web service

The `WSDL` folder contains the generated WSDL files from the JWS files.

# To Compile #

1. Copy the JWS files into `tomcat\webapps\axis`
2. Copy the `LTCManager.java` file into `tomcat\webapps\axis\WEB-INF\classes` and compile it with `javac LTCManager.java`
3. Start the tomcat instance with `catalina start`
4. Download the WSDL files from [http://localhost:8080/axis/MyLTCServer.jws?wsdl](http://localhost:8080/axis/MyLTCServer.jws?wsdl) and [http://localhost:8080/axis/MyLTCAdmin.jws?wsdl](http://localhost:8080/axis/MyLTCAdmin.jws?wsdl) into the `MyLTCServer` and `MyLTCAdmin` folders and rename to `MyLTCServer.xml` and `MyLTCAdmin.xml` respectively
5. Run `java org.apache.axis.wsdl.WSDL2Java MyLTCServer.xml` and `java org.apache.axis.wsdl.WSDL2Java MyLTCAdmin.xml` to generate the client side java stubs
6. Compile the two stub packages generated with `javac *.java`
7. Compile the client side applications with `javac MyUserClient.java` and `javac MyUserAdmin.java`

# To Run #

## MyUserClient ##

The following are available calls for the MyUserClient application.

1. `java MyUserClient currentOffset <locationName>`
2. `java MyUserClient listLocations`
2. `java MyUserClient convert <locationFrom> <locationTo> <locationFromTime>` The Time should be of the form HH:mm

## MyUserAdmin ##

The following are available calls for the MyUserAdmin application.

1. `java MyUserAdmin addLocation <username> <password> <locationName> <timeOffset>`. The time offset value is in decimal notation, not HH:mm
1. `java MyUserAdmin setOffset <username> <password> <locationName> <timeOffset>`. The time offset value is in decimal notation, not HH:mm
1. `java MyUserAdmin callCount <username> <password>`